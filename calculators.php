<?php
/**
 * Plugin Name:     Calculators
 * Plugin URI:      demo.gresak.net
 * Description:     Plugin with various calculators that you can add to pages using shortcodes
 * Author:          Gregor Gresak
 * Author URI:      https://gresak.net
 * Text Domain:     calculators
 * Domain Path:     /languages
 * Version:         2.1.1
 *
 * @package         Calculators
 */

require_once 'acf/acf.php';
require_once 'mustache/src/Mustache/Autoloader.php';
require_once 'cfd.php';

add_filter('acf/settings/show_admin', '__return_false');

Mustache_Autoloader::register();

$calc = Calculators::instance();

function calculator_properties($calc) {
	return get_object_vars($calc);
}

class Calculators {

	protected $version;

	protected $url; //deprecate it eventually

	public $baseurl;

	protected $options_post_id;

	protected $path;

	private static $instance;

	protected function __construct() {
		$this->set_version();
		$this->set_plugin_url();
		$this->set_path();
		//$this->add_option_page();
		add_action('after_setup_theme', array($this,'add_option_page'));
		add_action('wp_enqueue_scripts', array($this, "load_scripts"));
		add_shortcode( 'calculator', array($this,"calculators") );
	}

	public function calculators($atts) {
		if(empty($atts)) return;

		if(function_exists('get_fields')){
			$data = get_fields($atts[0]);
		}
		
		$file = $this->path."calculators/".$atts[0].".php";
		if(file_exists($file)) {
			return $this->render(file_get_contents($file),$data);
		}

		//include_once 
	}

	public function load_scripts() {
		wp_enqueue_script('jquery');
		wp_enqueue_style( "calculators_css", $this->url."css/style.css", false, $this->version );
	}

	protected function set_data($calc) {
		if(!function_exists('get_field')) return;

		$this->$calc = get_fields($calc);
	}

	protected function render($template,$data) {
		//should use mustache
		$m = new Mustache_Engine;

		$vars = calculator_properties($this);
		// if $data is not an array, the merge will return null so better test first
		if(is_array($data)) {
			$vars = array_merge($vars,$data);
		}

		return $m->render($template,$vars);
	}

	public function add_option_page() {
		if($this->disable_option_page()) return;
		if( function_exists('acf_add_options_page') ) {
			
			acf_add_options_page([
				'page_title' 	=> 'Sodobno suženjstvo',
				'menu_slug'		=> 'tiles',
				'capability'	=> 'manage_options',
				'icon_url'		=> 'dashicons-groups',
				'post_id'		=> 'tiles',
			]);
			
		}
	}

	protected function disable_option_page() {
		return apply_filters('disable_calculators_option_pages', false);
	}

	protected function set_plugin_url() {
		//nevermind ...
		$this->baseurl = $this->url = trailingslashit(plugin_dir_url(__FILE__));
	}

	protected function set_path() {
		$this->path = trailingslashit(dirname(__FILE__));
	}

	protected function set_version() {
		$theme = wp_get_theme();
		$wp_version = get_bloginfo( 'version' );
		$data = get_file_data(__FILE__,array('Version' => 'Version'));
		$this->version =  $wp_version ."." . $data['Version'];
	}

	public static function instance() {
		if (null === static::$instance) {
            static::$instance = new static();
        }
        
        return static::$instance;
	}

}
