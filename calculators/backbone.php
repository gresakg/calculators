<style>
    #map {
    position: relative;
    width:450px;
    height:550px;
    border:solid #ccc 1px;
    border-radius: 5px;
    margin:1em;
    background-image: url('{{baseurl}}/img/blue.jpg');
}

#interactive-left {   
    width:250px;
    float:left;
}

#map header {
    color:white;
    margin:1em;
    width:100%;
}

#map h1 {
    margin:.5em 0 0 0;
    font-size:1.2em;
    padding:0;
    color:white;
}

#interactive-left h2 {
    margin:0 0 .5em 0;
    font-size:1.1em;
    padding:0;
}

#map p {
    font-size:16px;
}

#map footer {
    position: absolute;
    bottom:0;
    left:0;
    font-size:60%;
    padding-left: 1em;
    color: skyblue;
}

#map footer a {
    color:skyblue;
}

#legend {
    height:340px;
    width:100%;
    border:solid #ccc 1px;
    border-radius: 5px;
    margin:1em;
    background-color: white;
}
#image {
    background-image:url('{{baseurl}}/img/spinet.png');
    float:right;
    position:relative;
    height:512px;
    width:160px;
    margin-top:1.5em;
}

.legend-item {
    display:none;
    font-size: 13px;
    padding:1em;
}

a.spot {
    display: block;
    position: absolute;
    opacity:.5;
    text-decoration: none;
    border-radius: 10px;
}

a.spot:hover {
    background-color: pink;
    box-shadow: 0px 0px 10px 2px pink;  
}

</style>

<div id="map">
<div id="interactive-left">
<header>
<h1>Hrbtenica</h1>
<p>Potujte z miško po hrbtenici in se spoznajte z njenimi vplivi na telo</p>
</header>
<div id="legend">
<div id="c1" class="legend-item">
<h2>Vratno vretence C 1</h2>
<p>Vpliva na dovajanje krvi v glavo,hipofizo, kosti obraza, možgane, notranje in srednje uho, simpatični živcni sistem</p>
<p><b>Možni simptomi:</b> glavobol, nervoza, nespečnost, prehlad, povišan krvni tlak, migrena, koncentracija, težave s spominom, kronična utrujenost, težave z ravnotežjem, mravlljinčavost v glavi, šumenje v ušesih</p>
</div>
<div id="c2" class="legend-item">
<h2>Vratno vretence C 2</h2>
<p>Vpliva na oči, vidni živec,sinuse, mastoidne kosti, obrazne živce</p>
<p>
                        <b>Možni simptomi:</b> težave s sinusi, alergije, bolečine v predelu oči, bolečine v ušesih, nekateri primeri slepote, gluhost, jecljanje, škiljenje</p>
</div>
<div id="c3" class="legend-item">
<h2>Vratno vretence C 3</h2>
<p>Vpliva na obraz, zunanje uho, kosti obraza, zobe, obrazne živce</p>
<p>
                        <b>Možni simptomi:</b> nevralgija, nevritis, akne ali mozolji, ekscemi</p>
</div>
<div id="c4" class="legend-item">
<h2>Vratno vretence C 4</h2>
<p>Vpliva na usta, evstahijevo trobljo, ustnice, nos</p>
<p>
                        <b>Možni simptomi:</b> povišana temperatura, izcedek iz nosu, izguba sluha, pojav nosnih polipov</p>
</div>
<div id="c5" class="legend-item">
<h2>Vratno vretence C 5</h2>
<p>Vpliva na glasilke, vratne žleze, žrelo</p>
<p>
                        <b>Možni simptomi:</b> laringitis, hripavost, bolečine v grlu, težave s požiranjem</p>
</div>
<div id="c6" class="legend-item">
<h2>Vratno vretence C 6</h2>
<p>Vpliva na vratne mišice, ramena, mandeljne</p>
<p>
                        <b>Možni simptomi:</b> občutek trdega vratu, bolečine v gornjem delu rok, tonsilitis, kronični kašelj</p>
</div>
<div id="c7" class="legend-item">
<h2>Vratno vretence C 7</h2>
<p>Vpliva na šcitnico, ramena, laket, komolec</p>
<p>
                        <b>Možni simptomi:</b> bursitis, prehlad, težave s ščitnico, zbadanje v ramenih</p>
</div>
<div id="t1" class="legend-item">
<h2>Prsno vretence Th 1</h2>
<p>Vpliva na roke od lakta navzdol, vključno s pestmi, sklepi in prsti, požiralnik</p>
<p>
                        <b>Možni simptomi:</b> astma, kašelj, oteženo dihanje, zastoji dihanja, bolečine v rokah in dlaneh</p>
</div>
<div id="t2" class="legend-item">
<h2>Prsno vretence Th 2</h2>
<p>Vpliva na srce, vključno z zaklopkami, osrčnikom in koronalnimi arterijami</p>
<p>
                        <b>Možni simptomi:</b> funkcionalna stanja srca in določenja stanja pljuč</p>
</div>
<div id="t3" class="legend-item">
<h2>Prsno vretence Th 3</h2>
<p>Vpliva na pljuča, bronhij, prsni koš, dojke, bradavice</p>
<p>
                        <b>Možni simptomi:</b> bronhitis, plevritis, pljučnica, zastoji, gripa</p>
</div>
<div id="t4" class="legend-item">
<h2>Prsno vretence Th 4</h2>
<p>Vpliva na žolčni mehur in skupno izvodilo</p>
<p>
                        <b>Možni simptomi:</b> stanja žolčnika, zlatenica, herpes zoster</p>
</div>
<div id="t5" class="legend-item">
<h2>Prsno vretence Th 5</h2>
<p>Vpliva na Jetra, plexus sciaris, krvni obtok</p>
<p>
                        <b>Možni simptomi:</b> stanja jeter, povišana temperatura, težave s krvnim tlakom, šibka prekrvavitev, artritis</p>
</div>
<div id="t6" class="legend-item">
<h2>Prsno vretence Th 6</h2>
<p>Vpliva na trebuh</p>
<p>
                        <b>Možni simptomi:</b> težave s trebuhom kot so prebavne motnje, neprijeten občutek (mravljinčenje), bolečine, zgaga, dispepsija </p>
</div>
<div id="t7" class="legend-item">
<h2>Prsno vretence Th 7</h2>
<p>Vpliva na trebušno slinavko, dvanajsternik</p>
<p>
                        <b>Možni simptomi:</b> razjede, gastritis</p>
</div>
<div id="t8" class="legend-item">
<h2>Prsno vretence Th 8</h2>
<p>Vpliva na vranico, prsno prepono</p>
<p>
                        <b>Možni simptomi:</b> zmanjšana odpornost</p>
</div>
<div id="t9" class="legend-item">
<h2>Prsno vretence Th 9</h2>
<p>Vpliva na nadledvične žleze</p>
<p>
                        <b>Možni simptomi:</b> alergije, koprivnica</p>
</div>
<div id="t10" class="legend-item">
<h2>Prsno vretence Th 10</h2>
<p>Vpliva na ledvica</p>
<p>
                        <b>Možni simptomi:</b> težave z ledvicami, otrdelost arterij, kronična utrujenost, nevritis, pyelitis</p>
</div>
<div id="t11" class="legend-item">
<h2>Prsno vretence Th 11</h2>
<p>Vpliva na ledvica, sečevod</p>
<p>
                        <b>Možni simptomi:</b> težave s kožo kot so akne, mozolji, ekscemi</p>
</div>
<div id="t12" class="legend-item">
<h2>Prsno vretence Th 12</h2>
<p>Vpliva na tanko črevo, obrok limfe</p>
<p>
                        <b>Možni simptomi:</b> revmatizem, napenjanje, določeni tipi sterilnosti</p>
</div>
<div id="l1" class="legend-item">
<h2>Ledveno vretence L 1</h2>
<p>Vpliva na debelo črevo, dimeljske obročke</p>
<p>
                        <b>Možni simptomi:</b> zaprtje, kolitis, griža, driska, določene rupture in hernije</p>
</div>
<div id="l2" class="legend-item">
<h2>Ledveno vretence L 2</h2>
<p> Vpliva na slepo črevo, trebuh, zgornji del nog</p>
<p>
                        <b>Možni simptomi:</b> krči, oteženo dihanje, krčne žile</p>
</div>
<div id="l3" class="legend-item">
<h2>Ledveno vretence L 3</h2>
<p>Vpliva na spolne organe, moda in jajčnike, sečni mehur, kolena</p>
<p>
                        <b>Možni simptomi:</b> težave z mehurjem, menstrualne težave (boleče, neredne menstruacije), splav, inkontinenca, impotenca, težave s koleni</p>
</div>
<div id="l4" class="legend-item">
<h2>Ledveno vretence L 4</h2>
<p>Vpliva na prostato, mišice spodnjega dela hrbtenice, bedra</p>
<p>
                        <b>Možni simptomi:</b> išijas, lumbago, težavno, pekoče ali prepogosto uriniranje, bolečine v hrbtu</p>
</div>
<div id="l5" class="legend-item">
<h2>Ledveno vretence L 5</h2>
<p>Vpliva na spodnji del nog, sklepe stopal, prste na nogah</p>
<p>
                        <b>Možni simptomi:</b> šibka prekrvavitev nog, občutek šibkih gležnjev in meč, hladna stopala, krči</p>
</div>
<div id="sa" class="legend-item">
<h2>SAKRUM</h2>
<p>Vpliva na kosti kolka, stegna</p>
<p>
                        <b>Možni simptomi:</b> ukrivljenost hrbtenice, sakralno križni sklep</p>
</div>
<div id="co" class="legend-item">
<h2>COCCYX</h2>
<p>Vpliva na danko, anus, srbečico</p>
<p>
                        <b>Možni simptomi:</b> hemeroidi, srbečica, bolečine v trtici (ob sedenju)</p>
</div>
<footer>Interactive image code (c) <a href="http://gresak.net">gresak.net</a></footer>
</div>
</div>
<div id="image">
                <a href="#c1" class="spot" style="width:30px;height:10px;top:20px;left:80px">&nbsp;</a><br />
                <a href="#c2" class="spot" style="width:30px;height:10px;top:33px;left:80px">&nbsp;</a><br />
                <a href="#c3" class="spot" style="width:30px;height:10px;top:45px;left:80px">&nbsp;</a><br />
                <a href="#c4" class="spot" style="width:30px;height:10px;top:58px;left:80px">&nbsp;</a><br />
                <a href="#c5" class="spot" style="width:30px;height:12px;top:70px;left:80px">&nbsp;</a><br />
                <a href="#c6" class="spot" style="width:40px;height:12px;top:84px;left:70px">&nbsp;</a><br />
                <a href="#c7" class="spot" style="width:45px;height:12px;top:98px;left:65px">&nbsp;</a>
<p>                <a href="#t1" class="spot" style="width:50px;height:12px;top:110px;left:55px">&nbsp;</a><br />
                <a href="#t2" class="spot" style="width:52px;height:13px;top:124px;left:53px">&nbsp;</a><br />
                <a href="#t3" class="spot" style="width:52px;height:13px;top:138px;left:53px">&nbsp;</a><br />
                <a href="#t4" class="spot" style="width:50px;height:14px;top:152px;left:52px">&nbsp;</a><br />
                <a href="#t5" class="spot" style="width:48px;height:15px;top:166px;left:51px">&nbsp;</a><br />
                <a href="#t6" class="spot" style="width:47px;height:15px;top:182px;left:50px">&nbsp;</a><br />
                <a href="#t7" class="spot" style="width:47px;height:16px;top:198px;left:49px">&nbsp;</a><br />
                <a href="#t8" class="spot" style="width:47px;height:16px;top:216px;left:50px">&nbsp;</a><br />
                <a href="#t9" class="spot" style="width:49px;height:17px;top:232px;left:51px">&nbsp;</a><br />
                <a href="#t10" class="spot" style="width:48px;height:18px;top:249px;left:56px">&nbsp;</a><br />
                <a href="#t11" class="spot" style="width:48px;height:19px;top:267px;left:62px">&nbsp;</a><br />
                <a href="#t12" class="spot" style="width:50px;height:19px;top:286px;left:66px">&nbsp;</a></p>
<p>                <a href="#l1" class="spot" style="width:52px;height:17px;top:305px;left:71px">&nbsp;</a><br />
                <a href="#l2" class="spot" style="width:54px;height:19px;top:322px;left:73px">&nbsp;</a><br />
                <a href="#l3" class="spot" style="width:56px;height:22px;top:341px;left:73px">&nbsp;</a><br />
                <a href="#l4" class="spot" style="width:56px;height:23px;top:363px;left:73px">&nbsp;</a><br />
                <a href="#l5" class="spot" style="width:58px;height:21px;top:386px;left:66px">&nbsp;</a><br />
                <a href="#sa" class="spot" style="width:67px;height:55px;top:407px;left:44px">&nbsp;</a><br />
                <a href="#co" class="spot" style="width:22px;height:39px;top:462px;left:43px">&nbsp;</a>
            </p>
</div>
</div>
<script>
    //hrbtenica
jQuery(".spot").on("hover",function(){
   jQuery(jQuery(this).attr("href")).toggle(); 
});
</script>