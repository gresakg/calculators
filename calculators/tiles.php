<style>

.tiles {
	display:flex;
	/*max-width: 1020px;*/
	margin:0 auto;
	flex-wrap: wrap;
	/*position:relative;*/
	justify-content: space-between;
	box-sizing: border-box;
}

.tiles.container {
	padding:0;
}

.tile {
	margin: 10px;
}

/* entire container, keeps perspective */
.flip-container {
	perspective: 1000px;
	transform-style: preserve-3d;
}
	/*  UPDATED! flip the pane when hovered */
	.flip-container.hover .back {
		transform: rotateY(0deg);
	}
	.flip-container.hover .front {
	    transform: rotateY(180deg);
	}

.flip-container, .front, .back {
	width: 320px;
	height: 320px;
}

/* flip speed goes here */
.flipper {
	transition: 0.6s;
	transform-style: preserve-3d;

	position: relative;
}

/* hide back of pane during swap */
.front, .back {
	backface-visibility: hidden;
	transition: 0.6s;
	transform-style: preserve-3d;

	position: absolute;
	top: 0;
	left: 0;
	padding: 5px;
}

/*  UPDATED! front pane, placed above back */
.front {
	z-index: 2;
	transform: rotateY(0deg);
}

/* back, initially hidden pane */
.back {
	transform: rotateY(-180deg);
	padding: 2em;
	text-align: center;
}

/* 
	Some vertical flip updates 
*/
.vertical.flip-container {
	position: relative;
}

	.vertical .back {
		transform: rotateX(180deg);
	}

	.vertical.flip-container:hover .back {
	    transform: rotateX(0deg);
	}

	.vertical.flip-container:hover .front {
	    transform: rotateX(180deg);
	}

	.flipper > * {
		border: solid #c0c0c0 1px;
	}

.tile .flipper-extend {
	display: none;
	opacity: 0;
	animation: fade 3s reverse;
}

.tile.toggled .flipper-extend {
	display:block;
	position: fixed;
	z-index: 100000;
	background-color: rgba(0,0,0,.5);
	top:0;
	left:0;
	width:100%;
	height:100%;
	opacity:1;
	animation: fade 1s;
}

@keyframes fade {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

.tile .tile-content {
	margin:10% auto;
	width:0;
	height:0;
	max-width: 760px;
	background-color: white;
	padding:2em;
	z-index: 100001;
	border-radius:5px;
	animation: pop .5s reverse;
}

.tile.toggled .tile-content {
	width: 80%;
	height:70%;
	overflow: auto;
	animation: pop .5s;
}

@keyframes pop {
    from {
        width: 0;
        height:0;
    }

    to {
        width:80%;
        height:70%;
    }
}

.tile.read {
	filter: grayscale(100%);
}

.back h3 {
	text-align: center;
}

.buttonholder {
	margin-bottom: 2em;
	text-align: center;
}

.buttonholder button {
	background-color: #a91f24;
	color: #fff;
	padding:.5em 1em;
	border: none;
	border-radius: 5px;
	font-weight: bold;
	font-size: 120%;
}

.buttonholder button:hover {
	background-color: #c73c40;
}

.tiles h2, .tiles h3 {
	color:#1fa3ce;
}

.flipper .front:hover {
    border: 0;
    padding: 0;
    transition: all .5s;
}

.front img:hover {

    filter: opacity(80%) drop-shadow(0px 0px 5px #f0c5b5);
    transition: all .5s;

}

.flames {
    /*position: fixed;
    right: 16px;
    bottom: 5%;*/
    width: 50px;
}

.flame {
	width: 46px;
	filter: grayscale(100%);
	display: inline-block;
}

.flame.small {
	width:32px;
}

.flame.lit {
	filter:none;
}

@media (max-width: 458px) {
	.tiles {
		display:block;
	}

	.flames {
		width: 100%;
		position: fixed;
		bottom:0;
		text-align: center;
		z-index:100001;
	}

	.tile {
		margin:10px 0;
	}
}

	
</style>
<div class="tiles container" style="flex-wrap: nowrap;">
<div class="flames">
	{{#tiles}}
	<div class="flame">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 viewBox="0 0 511.269 511.269" style="enable-background:new 0 0 511.269 511.269;" xml:space="preserve">
	<path style="fill:#F3705A;" d="M140.367,465.067C116.9,438.4,93.434,410.667,78.5,377.6c-14.933-35.2-19.2-75.733-11.733-114.133
		s24.533-74.667,49.067-105.6c-2.133,26.667,7.467,54.4,25.6,74.667c-10.667-51.2,6.4-106.667,40.533-147.2S263.034,18.133,312.1,0
		c-24.533,25.6-27.733,66.133-18.133,100.267c9.6,34.133,29.867,64,48,94.933c18.133,30.933,35.2,62.933,36.267,98.133
		c9.6-18.133,20.267-36.267,26.667-56.533c6.4-20.267,9.6-41.6,4.267-61.867c19.2,23.467,29.867,46.933,35.2,76.8
		c5.333,29.867,4.267,60.8,1.067,90.667c-4.267,33.067-12.8,67.2-30.933,94.933c-21.333,33.067-55.467,56.533-92.8,69.333
		C255.567,518.4,190.5,508.8,140.367,465.067z"/>
	<path style="fill:#FFD15C;" d="M221.434,504.533C308.9,538.667,395.3,435.2,347.3,355.2c0-1.067-1.067-1.067-1.067-2.133
		c4.267,43.733-6.4,75.733-26.667,93.867c10.667-25.6,3.2-55.467-9.6-81.067c-12.8-24.533-30.933-46.933-44.8-70.4
		c-13.867-24.533-24.533-52.267-18.133-80c-25.6,19.2-43.733,48-51.2,78.933c-7.467,30.933-3.2,65.067,10.667,93.867
		c-16-11.733-27.733-30.933-28.8-51.2c-17.067,20.267-27.733,46.933-26.667,73.6C151.034,452.267,184.1,489.6,221.434,504.533z"/>
	</svg>
	</div>
	{{/tiles}}
</div>
<div class="tiles">
	{{#tiles}}
	<div class="tile">
		<div class="flip-container" onclick="this.classList.toggle('hover');">
			<div class="flipper">
				<div class="front">
					<img src="{{image}}" alt="rose">
				</div>
				<div class="back">
					<h3>{{title}}</h3>
					<p>{{description}}</p>
					<div class="buttonholder">
						<button>Preberi!</button>
					</div>
				</div>
			</div>
		</div>
		<div class="flipper-extend">
			<div class="tile-content">
				<h2>{{title}}</h2>
				{{{main_text}}}
				<div class="buttonholder">
					<button>Prebral sem!</button>
				</div>
			</div>
		</div>
	</div>
	{{/tiles}}
</div>
</div>
<div class="congrat tile">
	<div class="flipper-extend">
		<div class="tile-content">
			<h3>Čestitamo!</h3>
			<p>Prebrali ste vse vsebine in se poučili o tem, kako spoznati in kaj narediti, če naletite na žrtev trgovine z ljudmi!</p>
			<p>Znanja pa ni nikoli preveč! Če vas ta tematika zanima si lahko preberete še:</p>
			<ul>
				<li><a href="https://www.karitas.si/smernice-za-pogovor-z-zrtvami-trgovine-z-ljudmi">Smernice za pogovor z žrtvami trgovine z ljudmi (v 4 jezikih)</a></li>
				<li><a href="https://www.karitas.si/pomoc/pomoc-zrtvam-trgovine-z-ljudmi">Več o pomoči žrtvam trgovine z ljudmi</a></li>
				<li><a href="https://www.karitas.si/literatura-trgovina-z-ljudmi">Literatura in viri</a></li>
			</ul>
			{{#tiles}}
			<div class="flame small lit">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 511.269 511.269" style="enable-background:new 0 0 511.269 511.269;" xml:space="preserve">
			<path style="fill:#F3705A;" d="M140.367,465.067C116.9,438.4,93.434,410.667,78.5,377.6c-14.933-35.2-19.2-75.733-11.733-114.133
				s24.533-74.667,49.067-105.6c-2.133,26.667,7.467,54.4,25.6,74.667c-10.667-51.2,6.4-106.667,40.533-147.2S263.034,18.133,312.1,0
				c-24.533,25.6-27.733,66.133-18.133,100.267c9.6,34.133,29.867,64,48,94.933c18.133,30.933,35.2,62.933,36.267,98.133
				c9.6-18.133,20.267-36.267,26.667-56.533c6.4-20.267,9.6-41.6,4.267-61.867c19.2,23.467,29.867,46.933,35.2,76.8
				c5.333,29.867,4.267,60.8,1.067,90.667c-4.267,33.067-12.8,67.2-30.933,94.933c-21.333,33.067-55.467,56.533-92.8,69.333
				C255.567,518.4,190.5,508.8,140.367,465.067z"/>
			<path style="fill:#FFD15C;" d="M221.434,504.533C308.9,538.667,395.3,435.2,347.3,355.2c0-1.067-1.067-1.067-1.067-2.133
				c4.267,43.733-6.4,75.733-26.667,93.867c10.667-25.6,3.2-55.467-9.6-81.067c-12.8-24.533-30.933-46.933-44.8-70.4
				c-13.867-24.533-24.533-52.267-18.133-80c-25.6,19.2-43.733,48-51.2,78.933c-7.467,30.933-3.2,65.067,10.667,93.867
				c-16-11.733-27.733-30.933-28.8-51.2c-17.067,20.267-27.733,46.933-26.667,73.6C151.034,452.267,184.1,489.6,221.434,504.533z"/>
			</svg>
			</div>
			{{/tiles}}
			<br>
			<p>Hvala, ker vam ni vseeno!</p>
			<div class="buttonholder">
				<button>Konec</button>
			</div>
		</div>	
	</div>
</div>
<div class="credit">
	<p>Spletna stran je nastala v okviru Projekta <em>Zdrav, zavarovan in pravično plačan</em>, ki ga je podprlo Ministrstvo za zdravje (2019).</p>
</div>
<script>
	jQuery(".flipper button").click(function(){
		jQuery(this).closest('.tile').removeClass('read').addClass("toggled");
	});
	jQuery(".tile-content button").click(function(){
		jQuery(this).closest(".tile").removeClass("toggled").addClass("read");
		jQuery('.flame').not('.lit').first().addClass('lit');
		if(jQuery(".tile.read").size() == 9) {
			jQuery('.congrat.tile').addClass('toggled');
		}
	})
	jQuery(".flipper-extend").click(function(){
		jQuery(this).closest(".tile").removeClass("toggled");
	})
</script>